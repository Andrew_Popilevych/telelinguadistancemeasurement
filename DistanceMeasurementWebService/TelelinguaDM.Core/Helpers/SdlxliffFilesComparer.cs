﻿using DiffPlex;
using DiffPlex.DiffBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TelelinguaDM.Core.Models;
using HtmlDiff;

namespace TelelinguaDM.Core.Helpers
{
    /// <summary>
    /// Statiс class for comparing 2 sdlxliff files
    /// </summary>
    public static class SdlxliffFilesComparer
    {
        //TODO: Exception handling
        public  static SdlxliffFilesComparingResult CompareFiles(SdlxliffDocument firstFile, SdlxliffDocument secondFile, bool onlyMTSegments=false)
        {
            var filesComparingResult = new SdlxliffFilesComparingResult();
            List<Segment> firstFileTargetSegments = new List<Segment>();
            List<Segment> secondFileTargetSegments = new List<Segment>();
            List<Segment> firstFileSourceSegments = new List<Segment>();
            try
            {
                firstFileTargetSegments = onlyMTSegments ? firstFile.GetTargetSegments()
                                                                      .Where(s => s.IsMT)
                                                                      .ToList() : firstFile.GetTargetSegments();
                secondFileTargetSegments = secondFile.GetTargetSegments();
                firstFileSourceSegments = onlyMTSegments ? firstFile.GetSourceSegments()
                                                                        .Where(s => s.IsMT)
                                                                        .ToList() : firstFile.GetSourceSegments();
            }catch(Exception e)
            {
                throw;
            }
            

            var totalWordsNumber = 0;
            var totalTextLength = 0;
            var changedWords = 0;
            var totalEditDistance = 0;
            for (int i = 0; i < firstFileTargetSegments.Count; i++)
            {
                totalWordsNumber += firstFileTargetSegments[i].Value.GetWordsNumber();
                totalTextLength += firstFileTargetSegments[i].Value.Length;

                var appropriateSegment = secondFileTargetSegments.FirstOrDefault(s=>s.Id == firstFileTargetSegments[i].Id);

                if(appropriateSegment == null)
                {
                    continue;
                }
                filesComparingResult.SegmentsList.Add(firstFileTargetSegments[i]);
                var editDistance = firstFileTargetSegments[i].Value.CalculateDistance(appropriateSegment.Value, DistanceOptions.Character);
                totalEditDistance += editDistance;
                if (editDistance > 0)
                {
                   // var changedWordsInSegment = firstFileTargetSegments[i].Value.CalculateDistance(appropriateSegment.Value, DistanceOptions.Word);
                    //changedWords += changedWordsInSegment;
                    var wordBasedEditDistance = firstFileTargetSegments[i].Value.CalculateDistance(appropriateSegment.Value, DistanceOptions.Word);
                    //changedWords += wordBasedEditDistance;
                    var characterBasedweight = firstFileTargetSegments[i].Value.Length == 0 ? 0 : 1 - Math.Round((double)editDistance / firstFileTargetSegments[i].Value.Length, 4);
                    if (characterBasedweight < 0)
                    {
                        characterBasedweight = 0;
                    }
                    
                    var wordBasedWeight = firstFileTargetSegments[i].Value.GetWordsNumber() == 0 ? 0 : 1 - Math.Round((double)wordBasedEditDistance / firstFileTargetSegments[i].Value.GetWordsNumber(), 4);
                    if (wordBasedWeight < 0)
                    {
                        wordBasedWeight = 0;
                    }
                    var modifiedWords = GetModifiedWords(firstFileTargetSegments[i], appropriateSegment);
                    changedWords += modifiedWords.Where(w=>w.ModifiedPercent.CharacterBased>0).Count();
                    filesComparingResult.ModifiedSegments.Add(
                    new ModifiedSegment
                    {
                        OriginalSegment = firstFileTargetSegments[i],
                        ModidfiedSegment = GetModifiedSegment(firstFileTargetSegments[i], appropriateSegment),
                        SimilarityPercent = new Percent { CharacterBased = Math.Round(characterBasedweight*100),WordBased = Math.Round(wordBasedWeight*100)},
                        ModoficationPercent = new Percent { CharacterBased = Math.Round( 100 - characterBasedweight * 100) , WordBased = Math.Round(100 - wordBasedWeight * 100) },
                        ModifiedWords = modifiedWords,
                        EditDistance = editDistance
                    });
                    filesComparingResult.SourceSegments.Add(firstFileSourceSegments.FirstOrDefault(s=>s.Id == appropriateSegment.Id));
                }
            }
            
            filesComparingResult.ChangedWordsPercentage = totalWordsNumber == 0 ? 0: Math.Round((double)changedWords / totalWordsNumber * 100);
            filesComparingResult.SourceFileName = firstFile.FileName;
            filesComparingResult.TargetFileName = secondFile.FileName;
            filesComparingResult.TotalCharacterNumber = totalTextLength;
            filesComparingResult.TotalWordsNumber = totalWordsNumber;
            filesComparingResult.TotalSegmentsNumber = firstFileSourceSegments.Count;
            filesComparingResult.TotalModifiedWords = changedWords;
            filesComparingResult.TotalEditDistance = totalEditDistance;
            filesComparingResult.TotalWordsInModifiedSegments = GetTotalWordsNumberInModifiedSegments(filesComparingResult.ModifiedSegments);
            return filesComparingResult;
        }

        #region Helpers

        public static int GetSegmentNumberWithPercentSimiliratyWordBased(this SdlxliffFilesComparingResult FilesComparingResult,  double maxValue, double minValue)
        {
            return FilesComparingResult.ModifiedSegments.Count(sg => sg.SimilarityPercent.WordBased <= maxValue && sg.SimilarityPercent.WordBased >= minValue);
        }

        private static Segment GetModifiedSegment(Segment oldSegment,Segment newSegment)
        {
            
            var htmlDiff = new HtmlDiff.HtmlDiff(oldSegment.Value, newSegment.Value);
            var result = htmlDiff.Build();
            return new Segment
            {
                Id = newSegment.Id,
                Value = result,
                WordCount = newSegment.WordCount,
                MatchStatus = oldSegment.MatchStatus
            };

        }

        private static List<ModifiedWords> GetModifiedWords(Segment sourceSegment, Segment targetSegment)
        {
            var firstFileSegmentWords = sourceSegment.Value.SplitToWords();
            var secondFileSegmentWords = targetSegment.Value.SplitToWords();

            var modifiedWords = new List<ModifiedWords>();

            if(secondFileSegmentWords.Count == 0 && firstFileSegmentWords.Count > 0)
            {
                firstFileSegmentWords.ForEach(sw => modifiedWords.Add(new ModifiedWords
                {
                    ModifiedPercent = new Percent {CharacterBased = 100 },
                    SimilarityPercent = new Percent {CharacterBased = 0 },
                    SourceWord = sw,
                    TargetWord = string.Empty
                }));
                return modifiedWords;
            }

            for (int wordCount = 0; wordCount < Math.Min(firstFileSegmentWords.Count,secondFileSegmentWords.Count); wordCount++)
            {
                var distance = firstFileSegmentWords[wordCount].CalculateDistance(secondFileSegmentWords[wordCount], DistanceOptions.Character);
                var weight = firstFileSegmentWords[wordCount].Length == 0 ? 0 : 1 - Math.Round((double)distance / firstFileSegmentWords[wordCount].Length, 2);
                if (weight < 0)
                {
                    weight = 0;
                }
                var similarityPercent = Math.Round(weight * 100);
                var modifiedWord = new ModifiedWords
                {
                    SourceWord = firstFileSegmentWords[wordCount],
                    TargetWord = secondFileSegmentWords[wordCount],
                    ModifiedPercent = new Percent { CharacterBased = 100 - similarityPercent },
                    SimilarityPercent = new Percent { CharacterBased = similarityPercent}
                };
                modifiedWords.Add(modifiedWord);
            }
            return modifiedWords;

        }

        private static int GetTotalWordsNumberInModifiedSegments(List<ModifiedSegment> modifiedSegments)
        {
            var count = 0;
            modifiedSegments.ForEach(s => { count += s.ModifiedWords.Count; });
            return count;
        }

        #endregion
    }
}
