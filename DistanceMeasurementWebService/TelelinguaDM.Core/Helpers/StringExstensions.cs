﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TelelinguaDM.Core.Models;

namespace TelelinguaDM.Core.Helpers
{
    public enum DistanceOptions
    {
        Word, Character
    }
    public static class StringExstensions
    {
        /// <summary>
        /// Extension for calculating edit distance with Damerau-Levenshtein algorithm
        /// </summary>
        /// <param name="sourceString">first string</param>
        /// <param name="targetString">second string</param>
        /// <returns>Number of unit operations necessary to obtain targetString from sourceString</returns>
        public static int CalculateDistance(this string sourceString, string targetString, DistanceOptions options)
        {
            if(sourceString == null || targetString == null)
            {
                throw new ArgumentNullException("Strings can't be null");
            }

            if( options == DistanceOptions.Character)
            {
                return DamerauLevenshtainDistance(sourceString, targetString);
            }
            else
            {

                return DamerauLevenshtainDistanceWordBased(sourceString, targetString);
                //var sourceStringWords = sourceString.SplitToWords();
                //var targetStringWords = targetString.SplitToWords();
                //int changedWords = 0;
                //if(targetStringWords.Count== 0 && sourceStringWords.Count > 0)
                //{
                //    return sourceStringWords.Count;
                //}
                //for(int i = 0; i < Math.Min(targetStringWords.Count,sourceStringWords.Count); i++)
                //{
                //    if (DamerauLevenshtainDistance(sourceStringWords[i], targetStringWords[i]) > 0)
                //    {
                //        changedWords++;
                //    }
                //}
                //return changedWords;
            }      
        }

        /// <summary>
        /// Gets amount of the wors in string
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static int GetWordsNumber(this string sourceString)
        {
            return new Regex(@"\w+\b").Matches(sourceString).Count;
        }

        /// <summary>
        /// Splits sentence into words
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns>List of words wich contains current string</returns>
        public static List<string> SplitToWords(this string sourceString)
        {
            var matchesCollection = new Regex(@"\w+\b").Matches(sourceString);
            return matchesCollection
                  .Select(match => match.Value)
                  .ToList();
        }

        public static void AddEmptyStringToCollection(this List<string> collection, int numberOfEmptyStrings)
        {
            for(int i = 0; i < numberOfEmptyStrings; i++)
            {
                collection.Add(string.Empty);
            }
            
        }

       
        #region Helpers
        /// <summary>
        /// Damerau-Levenshtein algorithm implementation
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        private static int DamerauLevenshtainDistance(string source, string target)
        {
            if (source.Length == 0) return target.Length;
            if (target.Length == 0) return source.Length;

            var matrix = new int[source.Length + 1, target.Length + 1];
            //setting first column with values
            for (int i = 0; i <= source.Length; i++)
            {
                matrix[i, 0] = i;
            }
            //setting first row with values
            for (int j = 0; j <= target.Length; j++)
            {
                matrix[0, j] = j;
            }

            for (int i = 1; i <= source.Length; i++)
            {
                for (int j = 1; j <= target.Length; j++)
                {
                    var cost = source[i-1]==target[j-1] ? 0 : 1;
                    //finding min value
                    matrix[i, j] = Math.Min(Math.Min(matrix[i - 1, j] + 1, matrix[i, j - 1]+1), matrix[i - 1, j - 1] + cost);

                    if (i > 1 && j > 1 && source[i-1]==target[j - 2] && source[i - 2]==target[j-1])
                    {
                        matrix[i, j] = Math.Min(matrix[i, j], matrix[i - 2, j - 2] + cost);
                    }
                }
            }

            return matrix[source.Length, target.Length];

        }

        private static int DamerauLevenshtainDistanceWordBased(string source, string target)
        {
            if (source.Length == 0) return target.Length;
            if (target.Length == 0) return source.Length;
            var sourceStringWords = source.SplitToWords();
            var targetStringWords = target.SplitToWords();

            var matrix = new int[sourceStringWords.Count + 1, targetStringWords.Count + 1];
            //setting first column with values
            for (int i = 0; i <= sourceStringWords.Count; i++)
            {
                matrix[i, 0] = i;
            }
            //setting first row with values
            for (int j = 0; j <= targetStringWords.Count; j++)
            {
                matrix[0, j] = j;
            }

            for (int i = 1; i <= sourceStringWords.Count; i++)
            {
                for (int j = 1; j <= targetStringWords.Count; j++)
                {
                    var cost = sourceStringWords[i - 1] == targetStringWords[j - 1] ? 0 : 1;
                    //finding min value
                    matrix[i, j] = Math.Min(Math.Min(matrix[i - 1, j] + 1, matrix[i, j - 1] + 1), matrix[i - 1, j - 1] + cost);

                    if (i > 1 && j > 1 && sourceStringWords[i - 1] == targetStringWords[j - 2] && sourceStringWords[i - 2] == targetStringWords[j - 1])
                    {
                        matrix[i, j] = Math.Min(matrix[i, j], matrix[i - 2, j - 2] + cost);
                    }
                }
            }

            return matrix[sourceStringWords.Count, targetStringWords.Count];
        }
        #endregion
    }
}
