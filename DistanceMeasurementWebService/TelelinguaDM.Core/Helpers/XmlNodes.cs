﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelelinguaDM.Core.Helpers
{
    /// <summary>
    /// XPath definitions
    /// </summary>
    public static class XmlNodes
    {
        public const string TargetSegmentNode = "//xlf:target/descendant::xlf:mrk[@mtype='seg']";
        public const string SourceSegmentNode = "//xlf:seg-source/descendant::xlf:mrk[@mtype='seg']";
        public const string SegmentDefinitionNode = "//xlf:trans-unit/descendant::sdl:seg-defs/descendant::sdl:seg";
    }
}
