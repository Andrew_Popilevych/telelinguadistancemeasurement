﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using TelelinguaDM.Core.Helpers;
using TelelinguaDM.Core.Models;

namespace TelelinguaDM.Core
{
    //public class SdlxliffDocument
    //{
    //    #region Fields
    //    private XmlNamespaceManager _namespaceManager;
    //    //private List<XmlNode> _segmentDefinitionMode;
    //    #endregion

    //    #region Properties
    //    public XmlDocument DocumentContent { get; private set; } = new XmlDocument();

    //    public string FileName { get; private set; }
    //    #endregion

    //    #region Constructor

    //    public SdlxliffDocument(Stream stream, string fileName)
    //    {
    //        DocumentContent.Load(stream);
    //        SetNamespaces();
    //        FileName = fileName;
    //       // _segmentDefinitionMode = DocumentContent.SelectNodes(XmlNodes.SegmentDefinitionNode, _namespaceManager).Cast<XmlNode>().ToList();
    //    }

    //    public SdlxliffDocument(string xml, string fileName)
    //    {
    //        DocumentContent.LoadXml(xml);
    //        SetNamespaces();
    //        FileName = fileName;
    //       // _segmentDefinitionMode = 
    //            DocumentContent.SelectNodes(XmlNodes.SegmentDefinitionNode, _namespaceManager);
    //    }

    //    #endregion

    //    #region Methods
    //    /// <summary>
    //    /// Gets target segment number
    //    /// </summary>
    //    /// <returns></returns>
    //    public int GetSegmentsNumber()
    //    {
    //        var segments = DocumentContent.SelectNodes(XmlNodes.TargetSegmentNode, _namespaceManager);
    //        return segments.Count;
    //    }

    //    /// <summary>
    //    /// Gets list of target segments values
    //    /// </summary>
    //    /// <returns></returns>
    //    public List<Segment> GetTargetSegments()
    //    {
    //        DocumentContent.XmlResolver = null;
    //        var segmentsList = new List<Segment>();
          
    //        var segmentNodes = DocumentContent.SelectNodes(XmlNodes.TargetSegmentNode, _namespaceManager);
           
            
    //        for (int i=0;i< segmentNodes.Count;i++)
    //        {
    //            var id = segmentNodes[i].Attributes["mid"].Value;
    //            var nodeValue = segmentNodes[i].InnerText;
    //            var wordCount = nodeValue.SplitToWords().Count;
    //            var matchStatus = GetMatchStatusForSegment(id);
    //            segmentsList.Add(new Segment
    //            {
    //                Id = id,
    //                Value = nodeValue,
    //                WordCount = wordCount,
    //                MatchStatus = matchStatus,
    //                IsMT = IsMT(id)
    //            });
    //        }
            
    //        return segmentsList;
    
    //    }

    //    /// <summary>
    //    /// Gets list of source segments values
    //    /// </summary>
    //    /// <returns></returns>
    //    public List<Segment> GetSourceSegments()
    //    {
    //        DocumentContent.XmlResolver = null;
    //        var segmentsList = new List<Segment>();
            
    //        var segmentNodes = DocumentContent.SelectNodes(XmlNodes.SourceSegmentNode, _namespaceManager);
    //       // var segmentsDefinitions = DocumentContent.SelectNodes(XmlNodes.SegmentDefinitionNode, _namespaceManager);


    //        foreach (XmlNode segment in segmentNodes)
    //        {
    //            var id = segment.Attributes["mid"].Value;

    //            segmentsList.Add(new Segment
    //            {
    //                Id = id,
    //                Value = segment.InnerText,
    //                WordCount = segment.InnerText.SplitToWords().Count,
    //                MatchStatus = GetMatchStatusForSegment(id),
    //                IsMT = IsMT(id)
    //            });
    //        }
    //        return segmentsList;
    //    }

    //    #endregion

    //    #region Helpers
    //    private void SetNamespaces()
    //    {
    //        _namespaceManager = new XmlNamespaceManager(DocumentContent.NameTable);
    //        _namespaceManager.AddNamespace("sdl", "http://sdl.com/FileTypes/SdlXliff/1.0");
    //        _namespaceManager.AddNamespace("xlf", "urn:oasis:names:tc:xliff:document:1.2");
    //    }
        
    //    private bool IsMT(string id)
    //    {

    //        var xmlNode = _segmentDefinitionMode.FirstOrDefault(x => x.Attributes["id"].Value == id);
    //        var origin = xmlNode.Attributes["origin"] == null ? string.Empty : xmlNode.Attributes["origin"].Value;
    //        return origin == "mt" || origin == "amt";
    //    }
    //    private MatchStatus GetMatchStatusForSegment(string id)
    //    {
    //        var xmlNode = _segmentDefinitionMode.FirstOrDefault(x => x.Attributes["id"].Value == id);
            
    //        if (xmlNode!=null)
    //        {
    //            var origin = xmlNode.Attributes["origin"]==null?string.Empty: xmlNode.Attributes["origin"].Value;
    //            var textMatch = xmlNode.Attributes["text-match"]==null?string.Empty: xmlNode.Attributes["text-match"].Value;
    //            var percent = xmlNode.Attributes["percent"]==null?string.Empty: xmlNode.Attributes["percent"].Value;
    //            if (origin == "mt" || origin == "amt")
    //            {
    //                return MatchStatus.AT;
    //            }else if(origin == "interactive" && percent=="100" && textMatch=="SourceAndTarget")
    //            {
    //                return MatchStatus.ContextMatch;
    //            }else if(((origin == "interactive" || origin == "tm") && !string.IsNullOrEmpty(percent))){
    //                return GetPercentageMatchStatus(percent);
    //            }else if(origin == "document-match")
    //            {
    //                return MatchStatus.PerfectMatch;
    //            }else if(string.IsNullOrEmpty(origin)||origin == "not-translated")
    //            {
    //                return MatchStatus.NotTranslated;
    //            }else
    //            {
    //                return MatchStatus.New;
    //            }
    //        }
    //        else
    //        {
    //            return MatchStatus.NotTranslated; 
    //        }
    //    }

    //    private MatchStatus GetPercentageMatchStatus(string percentValueStr)
    //    {
    //        var percentValue = int.Parse(percentValueStr);
    //        if(percentValue == 100)
    //        {
    //            return MatchStatus.PercentValue100;
    //        }else if(percentValue>=95 && percentValue <= 99)
    //        {
    //            return MatchStatus.PercentValues95_99;
    //        }
    //        else if (percentValue >= 85 && percentValue <= 94)
    //        {
    //            return MatchStatus.PercentValues85_94;
    //        }
    //        else if (percentValue >= 74 && percentValue <= 85)
    //        {
    //            return MatchStatus.PercentValues75_84;
    //        }
    //        else if (percentValue >= 50 && percentValue <= 74)
    //        {
    //            return MatchStatus.PercentValues50_74;
    //        }
    //        else
    //        {
    //            return MatchStatus.PercentageValues0_50;
    //        }
    //    }
    //    #endregion
    //}

    public class SdlxliffDocument
    {
        #region Fields
        private XmlNamespaceManager _namespaceManager;
        #endregion

        #region Properties
        public XDocument DocumentContent { get; private set; } 

        public string FileName { get; private set; }
        #endregion

        #region Constructor

        public SdlxliffDocument(Stream stream, string fileName)
        {
            DocumentContent = XDocument.Load(stream);
            SetNamespaces();
            FileName = fileName;
        }
        
        #endregion

        #region Methods
        /// <summary>
        /// Gets target segment number
        /// </summary>
        /// <returns></returns>
        public int GetSegmentsNumber()
        {
            var segments = DocumentContent.XPathSelectElements(XmlNodes.TargetSegmentNode, _namespaceManager);
            return segments.Count();
        }

        /// <summary>
        /// Gets list of target segments values
        /// </summary>
        /// <returns></returns>
        public List<Segment> GetTargetSegments()
        {
            var segmentNodes = DocumentContent.XPathSelectElements(XmlNodes.TargetSegmentNode, _namespaceManager);
            int i = 0;
            var segmentsList = segmentNodes.Select(s =>
            {
                try
                {
                    var id = s.Attribute("mid").Value;
                    var nodeValue = s.Value;
                    var wordCount = nodeValue.SplitToWords().Count;
                    var segmentDefinitionNode = GetSegmentDefinitionNode(s);
                    var matchStatus = GetMatchStatusForSegment(segmentDefinitionNode);
                    var isMT = IsMT(segmentDefinitionNode);
                    i++;
                    return new Segment
                    {
                        Id = id,
                        Value = nodeValue,
                        WordCount = wordCount,
                        MatchStatus = matchStatus,
                        IsMT = isMT
                    };
                    
                }
                catch (Exception e)
                {
                    throw;
                }
               
            });
            
            return segmentsList.ToList();

        }

        /// <summary>
        /// Gets list of source segments values
        /// </summary>
        /// <returns></returns>
        public List<Segment> GetSourceSegments()
        {
            var segmentNodes = DocumentContent.XPathSelectElements(XmlNodes.SourceSegmentNode, _namespaceManager);
            var segmentsList = segmentNodes.Select(s =>
            {
                try
                {
                    var id = s.Attribute("mid").Value;
                    var nodeValue = s.Value;
                    var wordCount = nodeValue.SplitToWords().Count;
                    var segmentDefinitionNode = GetSegmentDefinitionNode(s);
                    var matchStatus = GetMatchStatusForSegment(segmentDefinitionNode);
                    return new Segment
                    {
                        Id = id,
                        Value = nodeValue,
                        WordCount = wordCount,
                        MatchStatus = matchStatus,
                        IsMT = IsMT(segmentDefinitionNode)
                    };
                }
                catch(Exception e)
                {
                    throw;
                }
                
            });
            return segmentsList.ToList();
        }

        #endregion

        #region Helpers
        private void SetNamespaces()
        {
            _namespaceManager = new XmlNamespaceManager(new NameTable());
            _namespaceManager.AddNamespace("sdl", "http://sdl.com/FileTypes/SdlXliff/1.0");
            _namespaceManager.AddNamespace("xlf", "urn:oasis:names:tc:xliff:document:1.2");
        }
        private XElement GetSegmentDefinitionNode(XElement node)
        {
            var id = node.Attribute("mid").Value;
            while (node.Name.LocalName != "trans-unit")
            {
                node = node.Parent;
                if (node == null)
                {
                    return null;
                }
            }
            var ns = _namespaceManager.LookupNamespace("sdl");
            var xmlNode = node.Element(XName.Get("seg-defs", ns))
                              .Elements(XName.Get("seg", ns))
                              .FirstOrDefault(n => n.Attribute("id").Value == id);
            return xmlNode;
        }
        private bool IsMT(XElement xmlNode)
        {
            var origin = xmlNode.Attribute("origin") == null ? string.Empty : xmlNode.Attribute("origin").Value;
            return origin == "mt" || origin == "amt";
        }
        private MatchStatus GetMatchStatusForSegment(XElement xmlNode)
        {
            if (xmlNode != null)
            {
                var origin = xmlNode.Attribute("origin") == null ? string.Empty : xmlNode.Attribute("origin").Value;
                var textMatch = xmlNode.Attribute("text-match") == null ? string.Empty : xmlNode.Attribute("text-match").Value;
                var percent = xmlNode.Attribute("percent") == null ? string.Empty : xmlNode.Attribute("percent").Value;
                if (origin == "mt" || origin == "amt")
                {
                    return MatchStatus.AT;
                }
                else if (origin == "interactive" && percent == "100" && textMatch == "SourceAndTarget")
                {
                    return MatchStatus.ContextMatch;
                }
                else if (((origin == "interactive" || origin == "tm") && !string.IsNullOrEmpty(percent)))
                {
                    return GetPercentageMatchStatus(percent);
                }
                else if (origin == "document-match")
                {
                    return MatchStatus.PerfectMatch;
                }
                else if (string.IsNullOrEmpty(origin) || origin == "not-translated")
                {
                    return MatchStatus.NotTranslated;
                }
                else
                {
                    return MatchStatus.New;
                }
            }
            else
            {
                return MatchStatus.NotTranslated;
            }
        }

        private MatchStatus GetPercentageMatchStatus(string percentValueStr)
        {
            var percentValue = int.Parse(percentValueStr);
            if (percentValue == 100)
            {
                return MatchStatus.PercentValue100;
            }
            else if (percentValue >= 95 && percentValue <= 99)
            {
                return MatchStatus.PercentValues95_99;
            }
            else if (percentValue >= 85 && percentValue <= 94)
            {
                return MatchStatus.PercentValues85_94;
            }
            else if (percentValue >= 74 && percentValue <= 85)
            {
                return MatchStatus.PercentValues75_84;
            }
            else if (percentValue >= 50 && percentValue <= 74)
            {
                return MatchStatus.PercentValues50_74;
            }
            else
            {
                return MatchStatus.PercentageValues0_50;
            }
        }
        #endregion

    }
}
