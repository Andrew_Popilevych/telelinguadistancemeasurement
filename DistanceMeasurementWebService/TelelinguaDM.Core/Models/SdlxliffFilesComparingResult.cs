﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TelelinguaDM.Core.Models
{
    [Serializable]
    public class SdlxliffFilesComparingResult
    {
        /// <summary>
        /// Source file name
        /// </summary>
        public string SourceFileName { get; set; }

        /// <summary>
        /// Target file name
        /// </summary>
        public string TargetFileName { get; set; }

        public List<Segment> SegmentsList { get; set; } = new List<Segment>();
        /// <summary>
        /// Source segments
        /// </summary>
        public List<Segment> SourceSegments { get; set; } = new List<Segment>();

        /// <summary>
        /// Modified segments list
        /// </summary>
        public List<ModifiedSegment> ModifiedSegments { get; set; } = new List<ModifiedSegment>();


        public double ChangedWordsPercentage { get; set; }
        /// <summary>
        /// Total segments number
        /// </summary>
        public int TotalSegmentsNumber { get; set; }

        /// <summary>
        /// Total words number
        /// </summary>
        public int TotalWordsNumber { get; set; }

        /// <summary>
        /// Total character namber
        /// </summary>
        public int TotalCharacterNumber { get; set; }


        public int TotalEditDistance { get; set; }
        /// <summary>
        /// Amount of modified words
        /// </summary>
        public int TotalModifiedWords { get; set; }

        /// <summary>
        /// Amount of words in modified segments
        /// </summary>
        public int TotalWordsInModifiedSegments { get; set; }
        

    }
}
