﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TelelinguaDM.Core.Models
{
    [Serializable]
    public enum MatchStatus
    {
        PerfectMatch,
        ContextMatch,
        PercentValue100,
        PercentValues95_99,
        PercentValues85_94,
        PercentValues75_84,
        PercentageValues0_50,
        PercentValues50_74,
        New,
        AT,
        NotTranslated
    }
    [Serializable]
    public class Segment
    {
        public string Id { get; set; }
        public string Value { get; set; }
        public int WordCount { get; set; }
        public MatchStatus MatchStatus { get; set; }
        public bool IsMT { get; set; }
    }
}
