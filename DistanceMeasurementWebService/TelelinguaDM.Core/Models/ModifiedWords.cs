﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelelinguaDM.Core.Models
{
    [Serializable]
    public class ModifiedWords
    {
        public string SourceWord { get; set; }
        public string TargetWord { get; set; }
        public Percent SimilarityPercent { get; set; }
        public Percent ModifiedPercent { get; set; }
    }
}
