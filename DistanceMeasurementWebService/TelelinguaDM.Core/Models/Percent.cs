﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelelinguaDM.Core.Models
{
    [Serializable]
    public class Percent
    {
        /// <summary>
        /// Similarity/modififcation percent using word-based algorithm
        /// </summary>
        public double WordBased { get; set; }

        /// <summary>
        /// Similarity/modification percent using character based algorithm
        /// </summary>
        public double CharacterBased { get; set; }
    }
}
