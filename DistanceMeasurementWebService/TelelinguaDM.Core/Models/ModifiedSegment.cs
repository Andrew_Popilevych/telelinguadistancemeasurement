﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelelinguaDM.Core.Models
{
    [Serializable]
    public class ModifiedSegment
    {
        public Segment OriginalSegment { get; set; }

        public Segment ModidfiedSegment { get; set; }

        public Percent SimilarityPercent { get; set; }

        public Percent ModoficationPercent { get; set; }

        public List<ModifiedWords> ModifiedWords { get; set; } 

        public int EditDistance { get; set; }

    }
}
