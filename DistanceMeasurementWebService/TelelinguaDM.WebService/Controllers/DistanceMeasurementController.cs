﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TelelinguaDM.Core;
using TelelinguaDM.Core.Helpers;
using TelelinguaDM.Core.Models;
using TelelinguaDM.WebService.Helpers;
//using TelelinguaDM.WebService.Models;
using TelelinguaDM.WebService.Pages;
using TelelinguaDM.WebService.Services;

namespace DistanceMeasurementWebService.Controllers
{
    [Flags]
    public enum ReportType
    {
        Generic=0, MT=1
    }
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DistanceMeasurementController : ControllerBase
    {
        private readonly IPageRenderService _pageRenderService;

        public DistanceMeasurementController(IPageRenderService pageRenderService)
        {
            _pageRenderService = pageRenderService;
        }
        #region Api methods

        /// <summary>
        /// Compares two .sdlxliff files and creates html log file
        /// </summary>
        /// <param name="sourceFile">n version of file</param>
        /// <param name="targetFile">n+1 version of file</param>
        /// <returns></returns>
        [HttpPost]
        public async Task GenerateComparisonResultInHtml(IFormFile sourceFile, IFormFile targetFile,ReportType reportType)
        {
            try
            {
                if(IsRightFileFormat(sourceFile.FileName,"sdlxliff") && IsRightFileFormat(sourceFile.FileName, "sdlxliff"))
                {
                    var sourceDocument = new SdlxliffDocument(sourceFile.OpenReadStream(), sourceFile.FileName);
                    var targetDocument = new SdlxliffDocument(targetFile.OpenReadStream(), targetFile.FileName);
                    var filesCompareResult = SdlxliffFilesComparer.CompareFiles(sourceDocument, targetDocument,reportType == ReportType.MT ? true : false);
                    var e = filesCompareResult.ModifiedSegments.Where(s => s.ModidfiedSegment.MatchStatus == MatchStatus.NotTranslated).ToList();
                    var result = await _pageRenderService.RenderPageToString("GenericReport", new GenericReportModel { FilesComparingResult = filesCompareResult,IsMTReport = reportType == ReportType.MT ? true : false });

                    var bytesArray = Encoding.Default.GetBytes(result);
                    await CreateFileResponse(bytesArray,"html");
                }
            }
            catch (Exception e)
            {
                HttpContext.Response.StatusCode = 500;
            }
        }

        /// <summary>
        /// Compares two .sdlxliff files and 
        /// </summary>
        /// <param name="sourceFile">n version of file</param>
        /// <param name="targetFile">n+1 version of file</param>
        /// <returns></returns>
        [HttpPost]
        public async Task GenerateComparisonResultInXml(IFormFile sourceFile, IFormFile targetFile, ReportType reportType)
        {
            try
            {
                if (IsRightFileFormat(sourceFile.FileName, "sdlxliff") && IsRightFileFormat(sourceFile.FileName, "sdlxliff"))
                {
                    var sourceDocument = new SdlxliffDocument(sourceFile.OpenReadStream(), sourceFile.FileName);
                    var targetDocument = new SdlxliffDocument(targetFile.OpenReadStream(), targetFile.FileName);
                    var filesCompareResult = SdlxliffFilesComparer.CompareFiles(sourceDocument, targetDocument, reportType == ReportType.MT ? true : false);
                    var serializer = new XmlSerializer(typeof(SdlxliffFilesComparingResult));

                    using(var memoryStream = new MemoryStream())
                    {
                        serializer.Serialize(memoryStream, filesCompareResult);
                        var bytesInStream = memoryStream.ToArray();
                        await CreateFileResponse(bytesInStream,"xml");
                    }
                    
                }
            }
            catch (Exception e)
            {

            }
        }

        #endregion

        #region Helpers

        //private async Task<string> ReadDataFromUploadedFile(IFormFile file)
        //{
        //    using (var streamReader = new StreamReader(file.OpenReadStream()))
        //    {
        //        var fileContent = await streamReader.ReadToEndAsync();
        //        return fileContent;
        //    }
        //}

        private bool IsRightFileFormat(string fileName,string format)
        {
            var extension = fileName.Substring(fileName.LastIndexOf('.')+1);
            return extension == format;
        }

        private async Task CreateFileResponse(byte[] bytesArray, string extension)
        {
            if (bytesArray.Length > 0)
            {
                HttpContext.Response.ContentType = "application/force-download";
                var name = $"attachment; filename=EditDistanceLog_{DateTime.UtcNow:G}.{extension}";
                HttpContext.Response.Headers.Add("content-disposition", name);
                await HttpContext.Response.Body.WriteAsync(bytesArray, 0, bytesArray.Length);
            }
        }
        #endregion

    }
}
