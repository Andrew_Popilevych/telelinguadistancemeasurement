﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TelelinguaDM.WebService.Helpers
{
    public class FileUploadOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if(operation.OperationId.ToLower() == "apidistancemeasurementgeneratecomparisonresultinhtmlpost" || operation.OperationId.ToLower() == "apidistancemeasurementgeneratecomparisonresultinxmlpost")
            {
                operation.Parameters.Clear();

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "reportType",
                    In = "query",
                    Description = "Set report type",
                    Required = true,
                    Type = "string",
                    Enum = new List<object> { "Generic","MT"}
                });
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "sourceFile",
                    In = "formData",
                    Description = "Upload source file",
                    Required = true,
                    Type = "file"
                });
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "targetFile",
                    In = "formData",
                    Description = "Upload target file",
                    Required = true,
                    Type = "file"
                });
                operation.Consumes.Add("multipart/form-data");
            }
        }
    }
}
