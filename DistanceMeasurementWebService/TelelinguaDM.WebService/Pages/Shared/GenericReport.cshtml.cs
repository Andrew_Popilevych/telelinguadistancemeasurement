﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TelelinguaDM.Core.Models;

namespace TelelinguaDM.WebService.Pages
{
    public enum Algorithm
    {
        CharacterBased, WordBased
    }
    public class GenericReportModel : PageModel
    {
        
        #region Properties
        public SdlxliffFilesComparingResult FilesComparingResult { get; set; }
        public bool IsMTReport { get; set; }
        
        #region OverallScore
        public int SimilarityWordsInChangedSegment100PercentCount
        {
            get
            {
                return FilesComparingResult.TotalWordsNumber - FilesComparingResult.TotalModifiedWords;
            }
        }

        public int SimilarityWordsInChangedSegment99_95PercentCount
        {
            get
            {
                return GetWordsNumberBySimilarityCategory(99, 95);
            }
        }

        public int SimilarityWordsInChangedSegment94_85PercentCount
        {
            get
            {
                return GetWordsNumberBySimilarityCategory(94, 85);
            }
        }

        public int SimilarityWordsInChangedSegment84_75PercentCount
        {
            get
            {
                return GetWordsNumberBySimilarityCategory(84, 75);
            }
        }

        public int SimilarityWordsInChangedSegment74_50PercentCount
        {
            get
            {
                return GetWordsNumberBySimilarityCategory(74, 50);
            }
        }

        public int SimilarityWordsInChangedSegment49_0PercentCount
        {
            get
            {
                return GetWordsNumberBySimilarityCategory(49, 0);
            }
        }
        #endregion

        #region OverviewOfModifications
        #region Segments
        public int TotalSegmentNumberInPerfectMatch
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PerfectMatch);
        }

        public int TotalSegmentNumberInContextMatch
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.ContextMatch);
        }
       
        public int TotalSegmentNumberInPercent100Match
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PercentValue100);
        }
        public int TotalSegmentNumberInPercent95_99Match
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PercentValues95_99);
        }
        public int TotalSegmentNumberInPercent85_94Match
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PercentValues85_94);
        }

        public int TotalSegmentNumberInPercent75_84Match
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PercentValues75_84);
        }
        public int TotalSegmentNumberInPercent50_74Match
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PercentValues50_74);
        }

        public int TotalSegmentNumberInNewMatch
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.New);
        }
        public int TotalSegmentNumberInATMatch
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.AT);
        }
        public int TotalSegmentNumberInNTMatch
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.NotTranslated);
        }
        #endregion

        #region ModifiedSegments
        public int TotalModifiedSegmentNumberInPerfectMatch
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PerfectMatch, true);
        }

        public int TotalModifiedSegmentNumberInContextMatch
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.ContextMatch, true);
        }
        public int TotalModifiedSegmentNumberInPercent100Match
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PercentValue100, true);
        }
        public int TotalModifiedSegmentNumberInPercent95_99Match
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PercentValues95_99, true);
        }
        public int TotalModifiedSegmentNumberInPercent85_94Match
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PercentValues85_94, true);
        }

        public int TotalModifiedSegmentNumberInPercent75_84Match
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PercentValues75_84, true);
        }
        public int TotalModifiedSegmentNumberInPercent50_74Match
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.PercentValues50_74, true);
        }

        public int TotalModifiedSegmentNumberInNewMatch
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.New, true);
        }
        public int TotalModifiedSegmentNumberInATMatch
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.AT, true);
        }
        public int TotalModifiedSegmentNumberInNTMatch
        {
            get => GetSegmentsNumberByMatchStatus(MatchStatus.NotTranslated,true);
        }
        #endregion

        #region TotalWordsNumber
        public int TotalWordsNumberInPerfectMatchSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PerfectMatch);
        }

        public int TotalWordsNumberInContextMatchSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.ContextMatch);
        }
        public int TotalWordsNumberInPercent100MatchSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PercentValue100);
        }
        public int TotalWordsNumberInPercent95_99MatchSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PercentValues95_99);
        }
        public int TotalWordsNumberInPercent85_94MatchSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PercentValues85_94);
        }

        public int TotalWordsNumberInPercent75_84MatchSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PercentValues75_84);
        }
        public int TotalWordsNumberInPercent50_74MatchSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PercentValues50_74);
        }
       
        public int TotalWordsNumberInNewMatchSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.New);
        }
        public int TotalWordsNumberInATMatchSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.AT);
        }
        public int TotalWordsNumberInNTMatchSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.NotTranslated);
        }

        #endregion

        #region ModifiedWordsNumber
        public int TotalWordsNumberInPerfectMatchModifiedSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PerfectMatch,true);
        }

        public int TotalWordsNumberInContextMatchModifiedSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.ContextMatch, true);
        }
       
        public int TotalWordsNumberInPercent100MatchModifiedSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PercentValue100, true);
        }
        public int TotalWordsNumberInPercent95_99MatchModifiedSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PercentValues95_99, true);
        }
        public int TotalWordsNumberInPercent85_94MatchModifiedSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PercentValues85_94, true);
        }

        public int TotalWordsNumberInPercent75_84MatchModifiedSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PercentValues75_84, true);
        }
        public int TotalWordsNumberInPercent50_74MatchModifiedSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.PercentValues50_74, true);
        }

        public int TotalWordsNumberInNewMatchModifiedSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.New, true);
        }
        public int TotalWordsNumberInATMatchModifiedSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.AT, true);
        }
        public int TotalWordsNumberInNTMatchModifiedSegment
        {
            get => GetWordsNumberInSegmentByMatchStatus(MatchStatus.NotTranslated,true);
        }

        #endregion

        #region TotalCharacterNumber
        public int TotalCharacterNumberInPerfectMatchSegment
        {
            
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PerfectMatch);
        }

        public int TotalCharacterNumberInContextMatchSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.ContextMatch);
        }
       
        public int TotalCharacterNumberInPercent100MatchSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PercentValue100);
        }
        public int TotalCharacterNumberInPercent95_99MatchSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PercentValues95_99);
        }
        public int TotalCharacterNumberInPercent85_94MatchSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PercentValues85_94);
        }

        public int TotalCharacterNumberInPercent75_84MatchSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PercentValues75_84);
        }
        public int TotalCharacterNumberInPercent50_74MatchSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PercentValues50_74);
        }

        public int TotalCharacterNumberInNewMatchSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.New);
        }
        public int TotalCharacterNumberInATMatchSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.AT);
        }
        public int TotalCharacterNumberInNTMatchSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.NotTranslated);
        }

        #endregion

        #region ModifiedCharacter
        public int TotalCharacterNumberInPerfectMatchModifiedSegment
        {

            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PerfectMatch,true);
        }

        public int TotalCharacterNumberInContextMatchModifiedSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.ContextMatch, true);
        }
     
        public int TotalCharacterNumberInPercent100MatchModifiedSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PercentValue100, true);
        }
        public int TotalCharacterNumberInPercent95_99MatchModifiedSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PercentValues95_99, true);
        }
        public int TotalCharacterNumberInPercent85_94MatchModifiedSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PercentValues85_94, true);
        }

        public int TotalCharacterNumberInPercent75_84MatchModifiedSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PercentValues75_84, true);
        }
        public int TotalCharacterNumberInPercent50_74MatchModifiedSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.PercentValues50_74, true);
        }

        public int TotalCharacterNumberInNewMatchModifiedSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.New, true);
        }
        public int TotalCharacterNumberInATMatchModifiedSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.AT, true);
        }
        public int TotalCharacterNumberInNTMatchModifiedSegment
        {
            get => GetCharacterNumberInSegmentByMatchStatus(MatchStatus.NotTranslated, true);
        }
        #endregion
        #endregion

        #region SimilarityScores

        #region WordBased

        public int SimilaritySegments100PercentCountWordBased
        {
            get
            {
                return FilesComparingResult.TotalSegmentsNumber - FilesComparingResult.ModifiedSegments.Count;
            }
        }

        public int SimilaritySegments99_95PercentCountWordBased
        {
            get
            {
                return GetSectionNumberPerSimilarityCategory(99, 95, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments94_85PercentCountWordBased
        {
            get
            {
                return GetSectionNumberPerSimilarityCategory(94, 85, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments84_75PercentCountWordBased
        {
            get
            {
                return GetSectionNumberPerSimilarityCategory(84, 75, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments74_50PercentCountWordBased
        {
            get
            {
                return GetSectionNumberPerSimilarityCategory(74, 50, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments49_0PercentCountWordBased
        {
            get
            {
                return GetSectionNumberPerSimilarityCategory(49, 0, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments100PercentWordCountWordBased
        {
            get
            {
                return FilesComparingResult.TotalWordsNumber - FilesComparingResult.TotalWordsInModifiedSegments;
            }
        }

        public int SimilaritySegments99_95PercentWordCountWordBased
        {
            get
            {
                return GetWordsNumberInSectionPerSimilarityCategory(99, 95, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments94_85PercentWordCountWordBased
        {
            get
            {
                return GetWordsNumberInSectionPerSimilarityCategory(94, 85, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments84_75PercentWordCountWordBased
        {
            get
            {
                return GetWordsNumberInSectionPerSimilarityCategory(84, 75, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments74_50PercentWordCountWordBased
        {
            get
            {
                return GetWordsNumberInSectionPerSimilarityCategory(74, 50, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments49_0PercentWordCountWordBased
        {
            get
            {
                return GetWordsNumberInSectionPerSimilarityCategory(49, 0, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments100PercentCharacterCountWordBased
        {
            get
            {
                return FilesComparingResult.TotalCharacterNumber - FilesComparingResult.ModifiedSegments.Sum(s=>s.OriginalSegment.Value.Length);
            }
        }

        public int SimilaritySegments99_95PercentCharacterCountWordBased
        {
            get
            {
                return GetCharacterNumberInSectionPerSimilarityCategory(99, 95, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments94_85PercentCharacterCountWordBased
        {
            get
            {
                return GetCharacterNumberInSectionPerSimilarityCategory(94, 85, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments84_75PercentCharacterCountWordBased
        {
            get
            {
                return GetCharacterNumberInSectionPerSimilarityCategory(84, 75, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments74_50PercentCharacterCountWordBased
        {
            get
            {
                return GetCharacterNumberInSectionPerSimilarityCategory(74, 50, Algorithm.WordBased);
            }
        }

        public int SimilaritySegments49_0PercentCharacterCountWordBased
        {
            get
            {
                return GetCharacterNumberInSectionPerSimilarityCategory(49, 0, Algorithm.WordBased);
            }
        }

        #endregion

        #region CharaterBased
        public int SimilaritySegments100PercentCountCharacterBased
        {
            get
            {
                return FilesComparingResult.TotalSegmentsNumber - FilesComparingResult.ModifiedSegments.Count;
            }
        }
        
        public int SimilaritySegments99_95PercentCountCharacterBased
        {
            get
            {
                return GetSectionNumberPerSimilarityCategory(99, 95, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments94_85PercentCountCharacterBased
        {
            get
            {
                return GetSectionNumberPerSimilarityCategory(94, 85, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments84_75PercentCountCharacterBased
        {
            get
            {
                return GetSectionNumberPerSimilarityCategory(84, 75, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments74_50PercentCountCharacterBased
        {
            get
            {
                return GetSectionNumberPerSimilarityCategory(74, 50, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments49_0PercentCountCharacterBased
        {
            get
            {
                return GetSectionNumberPerSimilarityCategory(49, 0, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments100PercentWordCountCharacterBased
        {
            get
            {
                return FilesComparingResult.TotalWordsNumber-FilesComparingResult.TotalWordsInModifiedSegments;
            }
        }

        public int SimilaritySegments99_95PercentWordCountCharacterBased
        {
            get
            {
                return GetWordsNumberInSectionPerSimilarityCategory(99, 95, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments94_85PercentWordCountCharacterBased
        {
            get
            {
                return GetWordsNumberInSectionPerSimilarityCategory(94, 85, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments84_75PercentWordCountCharacterBased
        {
            get
            {
                return GetWordsNumberInSectionPerSimilarityCategory(84, 75, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments74_50PercentWordCountCharacterBased
        {
            get
            {
                return GetWordsNumberInSectionPerSimilarityCategory(74, 50, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments49_0PercentWordCountCharacterBased
        {
            get
            {
                return GetWordsNumberInSectionPerSimilarityCategory(49, 0, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments100PercentCharacterCountCharacterBased
        {
            get
            {
                return FilesComparingResult.TotalCharacterNumber - FilesComparingResult.ModifiedSegments.Sum(s => s.OriginalSegment.Value.Length);
            }
        }

        public int SimilaritySegments99_95PercentCharacterCountCharacterBased
        {
            get
            {
                return GetCharacterNumberInSectionPerSimilarityCategory(99, 95, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments94_85PercentCharacterCountCharacterBased
        {
            get
            {
                return GetCharacterNumberInSectionPerSimilarityCategory(94, 85, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments84_75PercentCharacterCountCharacterBased
        {
            get
            {
                return GetCharacterNumberInSectionPerSimilarityCategory(84, 75, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments74_50PercentCharacterCountCharacterBased
        {
            get
            {
                return GetCharacterNumberInSectionPerSimilarityCategory(74, 50, Algorithm.CharacterBased);
            }
        }

        public int SimilaritySegments49_0PercentCharacterCountCharacterBased
        {
            get
            {
                return GetCharacterNumberInSectionPerSimilarityCategory(49, 0, Algorithm.CharacterBased);
            }
        }
        #endregion

        #endregion
        
        #endregion

        public void OnGet()
        {
           
        }

        #region Helpers
        private int GetSegmentsNumberByMatchStatus(MatchStatus status, bool modifiedSegments = false)
        {
            return !modifiedSegments ?
                                    FilesComparingResult.SegmentsList
                                                        .Count(s => s.MatchStatus == status)
                                    : FilesComparingResult.ModifiedSegments
                                                          .Count(ms => ms.ModidfiedSegment.MatchStatus == status);
        }

        private int GetWordsNumberInSegmentByMatchStatus(MatchStatus status,bool modifiedSegments=false)
        {
            
            return !modifiedSegments ? FilesComparingResult.SegmentsList
                                                          .Where(s => s.MatchStatus == status)
                                                          .Sum(s => s.WordCount)
                                    : FilesComparingResult.SourceSegments
                                                          .Where(s => s.MatchStatus == status)
                                                          .Sum(s => s.WordCount);
        }

        private int GetCharacterNumberInSegmentByMatchStatus(MatchStatus status, bool modifiedSegments= false)
        {
            return !modifiedSegments ? FilesComparingResult.SegmentsList
                                                          .Where(s => s.MatchStatus == status)
                                                          .Sum(s => s.Value.Length)
                                    : FilesComparingResult.SourceSegments
                                    .Where(s => s.MatchStatus == status)
                                    .Sum(s=>s.Value.Length);
        }

        private int GetSectionNumberPerSimilarityCategory(double maxValue, double minValue,Algorithm algorithm)
        {
            return algorithm == Algorithm.CharacterBased ?
                                        FilesComparingResult.ModifiedSegments
                                                            .Count(ms => ms.SimilarityPercent.CharacterBased <= maxValue && ms.SimilarityPercent.CharacterBased >= minValue)
                                        : FilesComparingResult.ModifiedSegments
                                                            .Count(ms => ms.SimilarityPercent.WordBased <= maxValue && ms.SimilarityPercent.WordBased >= minValue);


        }

        //TODO:Source segments word count
        //TODO: 100 percent evaluation
        //TODO Check totals
        //TODO Diagrams
        
        private int GetWordsNumberInSectionPerSimilarityCategory(double maxValue, double minValue,Algorithm algorithm)
        {
            return algorithm == Algorithm.CharacterBased ? FilesComparingResult.ModifiedSegments
                                                                                .Where(ms => ms.SimilarityPercent.CharacterBased <= maxValue && ms.SimilarityPercent.CharacterBased >= minValue)
                                                                                .Sum(s => s.OriginalSegment.WordCount)
                                                         : FilesComparingResult.ModifiedSegments
                                                                                .Where(ms => ms.SimilarityPercent.WordBased <= maxValue && ms.SimilarityPercent.WordBased >= minValue)
                                                                                .Sum(s => s.OriginalSegment.WordCount);
        }

        private int GetCharacterNumberInSectionPerSimilarityCategory(double maxValue, double minValue, Algorithm algorithm)
        {
            return algorithm == Algorithm.CharacterBased ? FilesComparingResult.ModifiedSegments
                                                                                .Where(ms => ms.SimilarityPercent.CharacterBased <= maxValue && ms.SimilarityPercent.CharacterBased >= minValue)
                                                                                .Sum(s => s.OriginalSegment.Value.Length)
                                                         : FilesComparingResult.ModifiedSegments
                                                                                .Where(ms => ms.SimilarityPercent.WordBased <= maxValue && ms.SimilarityPercent.WordBased >= minValue)
                                                                                .Sum(s => s.OriginalSegment.Value.Length);
        }
        
        private int GetWordsNumberBySimilarityCategory(double maxValue, double minValue)
        {
            return FilesComparingResult.ModifiedSegments.Sum(ms => ms.ModifiedWords.Count(mw => mw.SimilarityPercent.CharacterBased <= maxValue && mw.SimilarityPercent.CharacterBased >= minValue));
        }

        public string ToDescriptionString(MatchStatus status)
        {
            switch (status)
            {
                case MatchStatus.AT: return "AT";
                case MatchStatus.ContextMatch: return "Context match";
                case MatchStatus.New: return "New";
                case MatchStatus.NotTranslated: return "Not translated";
                case MatchStatus.PercentageValues0_50: return "0%-50%";
                case MatchStatus.PercentValue100: return "100%";
                case MatchStatus.PercentValues50_74: return "50%-74%";
                case MatchStatus.PercentValues75_84: return "75%-84%";
                case MatchStatus.PercentValues85_94: return "85%-94%";
                case MatchStatus.PercentValues95_99: return "95%-99%";
                case MatchStatus.PerfectMatch: return "Perfect match";
                //case MatchStatus.Repetitions: return "Repetitions";
            }
            return string.Empty;
        }
        #endregion
    }
}