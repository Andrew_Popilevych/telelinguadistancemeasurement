﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TelelinguaDM.WebService.Services
{
    public interface IPageRenderService
    {
        /// <summary>
        /// Renders razor page to string
        /// </summary>
        /// <typeparam name="T">page model type</typeparam>
        /// <param name="pageName">page name</param>
        /// <param name="pageModel">page model</param>
        /// <returns></returns>
        Task<string> RenderPageToString<T>(string pageName, T pageModel) where T : PageModel;
    }
}
