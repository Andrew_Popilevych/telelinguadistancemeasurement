﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;

namespace TelelinguaDM.WebService.Services
{
    public class PageRenderService : IPageRenderService
    {
        #region Fields
        private readonly IRazorViewEngine _razorViewEngine;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IActionContextAccessor _actionContextAccessor;
        private readonly IRazorPageActivator _razorPageActivator;
        private readonly ITempDataProvider _tempDataProvider;
        #endregion

        #region Constructor
        public PageRenderService(IRazorViewEngine razorViewEngine,
                                 IHttpContextAccessor httpContextAccessor, 
                                 IActionContextAccessor actionContextAccessor,
                                 IRazorPageActivator razorPageActivator,
                                 ITempDataProvider tempDataProvider)
        {
            _razorViewEngine = razorViewEngine;
            _httpContextAccessor = httpContextAccessor;
            _actionContextAccessor = actionContextAccessor;
            _razorPageActivator = razorPageActivator;
            _tempDataProvider = tempDataProvider;
        }
        #endregion
        public async Task<string> RenderPageToString<T>(string pageName, T pageModel) where T : PageModel
        {
            var actionContext = new ActionContext
            {
                HttpContext = _httpContextAccessor.HttpContext,
                ActionDescriptor = _actionContextAccessor.ActionContext.ActionDescriptor,
                RouteData = _httpContextAccessor.HttpContext.GetRouteData()
            };

            using(var stringWriter = new StringWriter())
            {
                //finding requested page
                var razorPage = _razorViewEngine.FindPage(actionContext, pageName);

                if (razorPage.Page == null)
                {
                    throw new ArgumentNullException("Specified page was not found. Check whether it exists.");
                }
                //creating razor view
                var razorView = new RazorView(_razorViewEngine, _razorPageActivator, new List<IRazorPage>(), razorPage.Page, HtmlEncoder.Default, new DiagnosticListener("PageRenderService"));

                //creating view context
                var viewContext = new ViewContext(actionContext, razorView, new ViewDataDictionary<T>(new EmptyModelMetadataProvider(), new ModelStateDictionary()) { Model = pageModel }, new TempDataDictionary(_httpContextAccessor.HttpContext, _tempDataProvider), stringWriter, new HtmlHelperOptions());

                var page = (Page)razorPage.Page;

                page.PageContext = new PageContext
                {
                    ViewData = viewContext.ViewData
                };

                page.ViewContext = viewContext;

                _razorPageActivator.Activate(page, viewContext);
                await page.ExecuteAsync();
                return stringWriter.ToString();
            }
        }
    }
}
