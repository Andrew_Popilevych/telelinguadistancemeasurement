FROM microsoft/dotnet:2.1.401-sdk AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY . ./
RUN dotnet restore

# Copy everything else and build

RUN dotnet publish -c Release -o out

Run ls
# Build runtime image
WORKDIR /app
Run ls
COPY ./TelelinguaDM.WebService/appsettings.json .
# COPY --from=build-env ./TelelinguaDM.WebService/out/ .
ENTRYPOINT ["dotnet", "./TelelinguaDM.WebService/out/TelelinguaDM.WebService.dll"]